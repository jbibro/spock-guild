package com.travactory.guild.spock

import org.springframework.stereotype.Component

@Component
class BookingService(val emailService: EmailService) {
    fun confirm(username: String) {
        emailService.send(username)
    }
}

@Component
class EmailService {
    fun send(username: String) {}
}