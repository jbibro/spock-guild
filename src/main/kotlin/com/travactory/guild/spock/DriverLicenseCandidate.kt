package com.travactory.guild.spock

class DriverLicenseCandidate(val age: Int, val countryCode: String) {
    fun isEligible(): Boolean {
        return when (countryCode) {
            "PL" -> when (age) {
                in 1..17 -> false
                else -> true
            }
            "US" -> when (age) {
                in 1..15 -> false
                else -> true
            }
            else -> false
        }
    }

    fun isEligible(driverLicenceAgeLimits: DriverLicenceAgeLimits): Boolean {
        return age > driverLicenceAgeLimits.minAgeInCountry(countryCode)
    }
}