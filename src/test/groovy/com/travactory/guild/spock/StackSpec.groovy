package com.travactory.guild.spock

import spock.lang.Specification

class StackSpec extends Specification {
    def stack = new Stack()

    def "pushes element on stack"() {
        given: "stack with single item"
        stack.push(1)

        when: "I put another item"
        stack.push(2)

        then: "top item is 2"
        !stack.empty()
        stack.size() == 2
        stack.peek() == 2
    }

    def "pops item from stack"() {
        given:
        stack.push(1)

        when:
        stack.push(2)

        and:
        stack.pop()

        then:
        with(stack) {
            !empty
            size() == 1
            peek() == 1
        }
    }

    def "popping from empty stack throws exception"() {
        when:
        stack.pop()

        then:
        thrown(EmptyStackException)
//        notThrown(EmptyStackException)
    }

    def "peek returns correct item"() {
        given:
        stack.push(1)

        expect:
        1 == stack.peek()
    }
}
