package com.travactory.guild.spock

import groovyx.net.http.RESTClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.ApplicationContext
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AppSpec extends Specification{

    @Autowired
    ApplicationContext context

    @LocalServerPort
    def port

    def "app starts"() {
        expect:
        context != null
    }

    def "application starts with a good health"() {
        given:
        def appClient = new RESTClient("http://localhost:$port")

        when:
        def result = appClient.get(
                path: "/actuator/health"
        )

        then:
        result.status == 200
    }
}
