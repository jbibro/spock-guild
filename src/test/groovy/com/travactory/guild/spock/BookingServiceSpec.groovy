package com.travactory.guild.spock

import spock.lang.Specification

class BookingServiceSpec extends Specification {

    def emailService = Mock(EmailService)

    def "sends email"() {
        given:
        def bookingService = new BookingService(emailService)

        when:
        bookingService.confirm('user123')

        then:
//        1 * emailService.send('user123')

        1 * emailService.send({ it.size() > 3 })
    }
}
