package com.travactory.guild.spock

import spock.lang.Specification
import spock.lang.Unroll

class MathSpec extends Specification{
    @Unroll
    def "#a * #b = #c"() {
        expect:
        a * b == c

        where:
        a | b || c
        2 | 3 || 6
        4 | 3 || 12
    }

    @Unroll
    def "#a is even number"() {
        expect:
        a % 2 == 0

        where:
        a << [2, 4, 6]
    }
}
