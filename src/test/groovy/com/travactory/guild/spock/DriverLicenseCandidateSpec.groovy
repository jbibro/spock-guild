package com.travactory.guild.spock

import de.jodamob.kotlin.testrunner.SpotlinTestRunner
import org.junit.runner.RunWith
import spock.lang.Specification
import spock.lang.Unroll

@RunWith(SpotlinTestRunner.class)
class DriverLicenseCandidateSpec extends Specification {


    @Unroll
    def "check eligibility"() {
        given:
        def candidate = new DriverLicenseCandidate(age, countryCode)

        expect:
        candidate.isEligible() == isEligible

        where:
        age | countryCode || isEligible
        16  | 'US'        || true
        16  | 'PL'        || false
    }

    def "can take driver licence exam"() {
        given:
        def ageLimits = Mock(DriverLicenceAgeLimits)
        ageLimits.minAgeInCountry('PL') >> 18

        when:
        def eligible = new DriverLicenseCandidate(17, 'PL').isEligible(ageLimits)

        then:
        !eligible
    }
}
